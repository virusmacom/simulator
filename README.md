# Mobility Intelligence Code Challenge(Solution)
kindly refer the problem statement at 
https://github.com/door2door-io/mi-code-challenge/blob/master/README.md
The Solution uses Java(spring boot), Sqllite(simulation.db), jquery and for visualization here Maps.
It has functional and Integration test in the form of junits. 

## Code Description
- **Dbutil.java** This is the class having the getCountryNameinSimulationDB(which get the country name from the simulated db),getCountryboundingbox(to get the bounding box values from the simulated db),
    insertbookingdistanceinsqllite(which inserts the value in the booking_distance table).
- **Helper.java**  This is the class which is basically the converted version of the python script of the challenge. this uses all the class in util package and helperobject.
- **Simulator.java** This is the Main class through which the program starts.
- **MainController.java** This is the class which have the Rest Mapping there are total 7 exposed Rest endpoint which is clearly defined in swagger, have integrated it with it.
 Swagger Page
 http://localhost:8080/swagger-ui.html#/
 ![Screenshot](swagger.png)
- **simulation.db** sqllite database, storing the data.
- **index.html,hereMap.js,hereMap.css and index.js** This are the Ui page which visualize the pickup and dropoff points.
![Screenshot](Output.png)
- **DbutilTest.java, HelperTest.java** These are the test cases which check the functionality of individual function and collection of them.
- **integrationtest.py**  this is the python script which check the complete end to end service test. 


## Developer Run and Debug

- In Intellij editor  open the project
- on terminal of intellij run , mvn clean install
- finally run the simulator class directly or in the debug mode
![Screenshot](editor.png)
- we can directly deploy this by
   java target/simulator-1.0.jar (default port is 8080).
- form the terminal we may further use
        mvn spring-boot:run
        
 
## Extra Functionality

- we can directly call the api service with the bounding box
  example:
  http://localhost:8080/getridingpool?min_lon=13.34014892578125&min_lat=52.52791908000258&max_lon=13.506317138671875&max_lat=52.562995039558004&numberRequest=3
  further there is post option also
  http://localhost:8080/getridingpool
  
  Post request:
  
  {
    "boundingBox": {"min_longitude":13.34014892578125, 
  "min_latitude":52.52791908000258, 
  "max_longitude":13.506317138671875, 
  "max_latitude":52.562995039558004
  },
    "number_of_requests": 3
  }
 

- Directly access the country name from the database
   http://localhost:8080/getcountrynamesimulateddb
- popup at the pickup points showing the details of the point in the UI.
   ![Screenshot](popup.png)
   
   
    
   
