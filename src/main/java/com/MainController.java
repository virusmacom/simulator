package com;

import com.google.gson.Gson;
import com.helperobject.RequestObject;
import com.helperobject.sqlitedb.Dbutil;
import com.util.BoundingBox;
import com.util.Helper;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.IOException;
import java.util.Optional;


@RestController
@EnableSwagger2
public class MainController {


    Gson gson = new Gson();


    @PostMapping(
            value = "/postridingpool", consumes = "application/json", produces = "application/json")
    public String postridingdetails(@RequestBody String objects) throws IOException {
        RequestObject requestObject=  gson.fromJson(objects, RequestObject.class);
        ;
       System.out.print("....."+requestObject.getNumber_of_requests());
          Helper helper = new Helper(requestObject.getBoundingBox());
        return helper.simulate(requestObject.getNumber_of_requests());
    }



    @GetMapping(
            value = "/getridingpool", produces = "application/json")
    public String getridingdetails(@RequestParam Double min_lon,@RequestParam Double min_lat,
                                   @RequestParam Double max_lon, @RequestParam Double max_lat,
                                   @RequestParam Optional<Integer> numberRequest ) throws IOException {

        Helper helper = new Helper(new BoundingBox(min_lon,min_lat,max_lon,max_lat));
        numberRequest.orElse(10);
        return helper.simulate(numberRequest.get());
    }

    @GetMapping(
            value = "/getridingpoolpickup", produces = "application/json")
    public String getridingpickupdetails(@RequestParam Double min_lon,@RequestParam Double min_lat,
                                   @RequestParam Double max_lon, @RequestParam Double max_lat,
                                   @RequestParam Optional<Integer> numberRequest ) throws IOException {

        Helper helper = new Helper(new BoundingBox(min_lon,min_lat,max_lon,max_lat));
        numberRequest.orElse(10);
        return helper.simulatepickuppoints(numberRequest.get());
    }

    @GetMapping(
            value = "/getridingpooldropoff", produces = "application/json")
    public String getridingdropoffdetails(@RequestParam Double min_lon,@RequestParam Double min_lat,
                                         @RequestParam Double max_lon, @RequestParam Double max_lat,
                                         @RequestParam Optional<Integer> numberRequest ) throws IOException {

        Helper helper = new Helper(new BoundingBox(min_lon,min_lat,max_lon,max_lat));
        numberRequest.orElse(10);
        return helper.simulatedropoffpoints(numberRequest.get());
    }


    @GetMapping(
            value = "/getcountrynamesimulateddb", produces = "application/json")
    public String getcountrynamesimulateddb() throws IOException {
        return Dbutil.getCountryNameinSimulationDB();
    }


    @GetMapping(
            value = "/getridingpoolpickupbycountry", produces = "application/json")
    public String getridingpoolpickupbycountry(@RequestParam String countryName ,@RequestParam (required = false)Optional<Integer> numberofrequest) throws IOException {

        Helper helper = new Helper(Dbutil.getCountryboundingbox(countryName));
        numberofrequest.orElse(10); // default is 10
        // inserted into the database
        Dbutil.insertbookingdistanceinsqllite(countryName,helper.getBookingDistanceBinsforinserting(numberofrequest.get()));
        return helper.simulatepickuppoints(numberofrequest.get());
    }

    @GetMapping(
            value = "/getridingpooldropoffcountry", produces = "application/json")
    public String getridingpooldropoffcountry(@RequestParam String countryName,@RequestParam (required = false)Optional<Integer> numberofrequest) throws IOException {

        Helper helper = new Helper(Dbutil.getCountryboundingbox(countryName));
        numberofrequest.orElse(10);
        Dbutil.insertbookingdistanceinsqllite(countryName,helper.getBookingDistanceBinsforinserting(numberofrequest.get()));
        return helper.simulatedropoffpoints(numberofrequest.get());
    }
}
