package com.helperobject;

import java.util.List;

public class FeatureCollection0 {
    String type="FeatureCollection";

    public FeatureCollection0(List<FeaturesO> features) {
        this.features = features;
    }

    public List<FeaturesO> getFeatures() {
        return features;
    }

    List<FeaturesO> features;

}
