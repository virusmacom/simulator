package com.helperobject;

public class FeaturesO {
    String type;

    public FeaturesO(String type, PropertiesO properties, GeometryO geometry) {
        this.type = type;
        this.properties = properties;
        this.geometry = geometry;
    }

    PropertiesO properties;
    GeometryO geometry;

}
