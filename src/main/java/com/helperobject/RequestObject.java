package com.helperobject;

import com.util.BoundingBox;

public class RequestObject {
    BoundingBox boundingBox;

    public BoundingBox getBoundingBox() {
        return boundingBox;
    }

    public void setBoundingBox(BoundingBox boundingBox) {
        this.boundingBox = boundingBox;
    }

    public Integer getNumber_of_requests() {
        return number_of_requests;
    }

    public void setNumber_of_requests(Integer number_of_requests) {
        this.number_of_requests = number_of_requests;
    }

    Integer number_of_requests;
}
