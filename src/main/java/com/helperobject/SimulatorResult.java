package com.helperobject;

public class SimulatorResult {
    String booking_distance_bins;
    FeatureCollection0 most_popular_dropoff_points;
    FeatureCollection0 most_popular_pickup_points;

    public SimulatorResult(String booking_distance_bins, FeatureCollection0 most_popular_dropoff_points, FeatureCollection0 most_popular_pickup_points) {
        this.booking_distance_bins = booking_distance_bins;
        this.most_popular_dropoff_points = most_popular_dropoff_points;
        this.most_popular_pickup_points = most_popular_pickup_points;
    }
}
