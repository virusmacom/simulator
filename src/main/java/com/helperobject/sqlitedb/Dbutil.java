package com.helperobject.sqlitedb;

import com.google.gson.Gson;
import com.util.BoundingBox;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Dbutil {

    static Gson gson = new Gson();

    public static String getCountryNameinSimulationDB() {
        Connection conn = null;
        List<String> simulatedCountryName= new ArrayList<>();
        try {
            // db parameters
            String url = "jdbc:sqlite:simulation.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);
            String sql ="select region_name from region_bounding_box";

            System.out.println("Connection to SQLite has been established.");

            ResultSet resultSet = conn.createStatement().executeQuery(sql);


            while (resultSet.next()) {
                String s = resultSet.getString(1);
                simulatedCountryName.add(s);
            }



        } catch ( SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

        return gson.toJson(simulatedCountryName);

    }

    public static BoundingBox getCountryboundingbox(String countryName) {
        Connection conn = null;
        BoundingBox boundingBox = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:simulation.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);
            String sql ="select * from region_bounding_box where region_name= '"+countryName+"'";

            System.out.println("Connection to SQLite has been established.");

            ResultSet resultSet = conn.createStatement().executeQuery(sql);


            while (resultSet.next()) {
                Double min_lat = resultSet.getDouble(3);
                Double min_lon = resultSet.getDouble(4);
                Double max_lat = resultSet.getDouble(5);
                Double max_lon = resultSet.getDouble(6);
                boundingBox = new BoundingBox(min_lon,min_lat,max_lon,max_lat);
            }



        } catch ( SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

        return boundingBox;

    }




    public static void insertbookingdistanceinsqllite(String countryName,List<Integer> fromto) {
        Connection conn = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:simulation.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);
            String sql ="select region_id from region_bounding_box where region_name= '"+countryName+"'";

            System.out.println("Connection to SQLite has been established.");

            ResultSet resultSet = conn.createStatement().executeQuery(sql);
            String region_id="";

            while (resultSet.next()) {
                region_id = resultSet.getString(1);
            }

            sql=" INSERT INTO booking_distance (\n" +
                    "    region_id,\n" +
                    "    from_0_1,\n" +
                    "    from_1_2,\n" +
                    "    from_2_3,\n" +
                    "    from_3_4\n" +
                    ")\n" +
                    "VALUES (\n" +
                    "    '"+region_id+"'," +fromto.get(0)+","+fromto.get(1)+","+fromto.get(2)+","+fromto.get(3)+")";


            conn.createStatement().execute(sql);


        } catch ( SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }


    }

}
