package com.util;

public class BoundingBox {
    Double min_longitude;
    Double min_latitude;
    Double max_longitude;

    public BoundingBox() {
    }

    public BoundingBox(Double min_longitude, Double min_latitude, Double max_longitude, Double max_latitude) {
        this.min_longitude = min_longitude;
        this.min_latitude = min_latitude;
        this.max_longitude = max_longitude;
        this.max_latitude = max_latitude;
    }

    Double max_latitude;

}
