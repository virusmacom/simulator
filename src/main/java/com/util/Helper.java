package com.util;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.helperobject.*;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Helper {
    final ArrayList<Double> booking_distance_distribution = new ArrayList();
    final Integer max_popular_points = 10;
    static String path_to_stops = null;
    BoundingBox bounding_box = null;
    Gson gson = new Gson();


    private void intialize() throws IOException {
        path_to_stops = IOUtils.toString(Helper.class.getClassLoader().getResourceAsStream("berlin_stops.geojson"), StandardCharsets.UTF_8.name());
        booking_distance_distribution.add(0.2);
        booking_distance_distribution.add(0.1);
        booking_distance_distribution.add(0.3);
        booking_distance_distribution.add(0.4);

    }

    public Helper(BoundingBox bounding_box) throws IOException {
        intialize();
        this.bounding_box = bounding_box;
    }

    public FeatureCollection0 getRandomPoints(int numberOfSamplePoints, String Strokeforpickup_points) {
        JsonArray featureArray = new JsonParser().parse(path_to_stops).getAsJsonObject().get("features").getAsJsonArray();

        List<FeaturesO> totalFeatures = new ArrayList<>();
        System.out.print(featureArray.size());

        for (int i = 0; i < featureArray.size(); i++) {

            double longitude = featureArray.get(i).getAsJsonObject().get("geometry").getAsJsonObject().get("coordinates").getAsJsonArray().get(0).getAsDouble();
            double latitude = featureArray.get(i).getAsJsonObject().get("geometry").getAsJsonObject().get("coordinates").getAsJsonArray().get(1).getAsDouble();
            if (pointLiesinBoundingBox(longitude, latitude)) {
                List<Double> coordinates = new ArrayList<>();
                coordinates.add(longitude);
                coordinates.add(latitude);
                GeometryO tempGeometry = new GeometryO(featureArray.get(i).getAsJsonObject().get("geometry").
                        getAsJsonObject().get("type").getAsString(), coordinates);
                PropertiesO tempoProperties = new PropertiesO(featureArray.get(i).getAsJsonObject().
                        get("properties").getAsJsonObject().get("name").getAsString(),
                        featureArray.get(i).getAsJsonObject().
                                get("properties").getAsJsonObject().get("id").getAsString(), Strokeforpickup_points
                );
                FeaturesO tempFeature = new FeaturesO(featureArray.get(i).getAsJsonObject().get("type").getAsString(), tempoProperties,
                        tempGeometry);
                totalFeatures.add(tempFeature);

            }
        }


        List<FeaturesO> totalFeaturesResult = new ArrayList<>();

        Random rand = new Random();
        for (int i = 0; i < numberOfSamplePoints; i++) {
            totalFeaturesResult.add(totalFeatures.get(rand.nextInt(totalFeatures.size())));
        }
        FeatureCollection0 result = new FeatureCollection0(totalFeaturesResult);

        System.out.print(gson.toJson(result));

        return result;
    }


    public String simulate( Integer number_of_requests) {


        String strokedropoffpoint = "#E95C07";
        String strokepickuppoints = "#020000";
        Integer numberOfSamplePoints = (number_of_requests < max_popular_points) ? number_of_requests : max_popular_points;
        SimulatorResult result = new SimulatorResult(getBookingDistanceBins(number_of_requests),
                getRandomPoints(numberOfSamplePoints, strokedropoffpoint), getRandomPoints(numberOfSamplePoints, strokepickuppoints));

        return gson.toJson(result);
    }

    public String simulatedropoffpoints( Integer number_of_requests) {


        String strokedropoffpoint = "#E95C07";
        Integer numberOfSamplePoints = (number_of_requests < max_popular_points) ? number_of_requests : max_popular_points;
        return gson.toJson(getRandomPoints(numberOfSamplePoints, strokedropoffpoint));
    }

    public String simulatepickuppoints( Integer number_of_requests) {
        String strokepickuppoints = "#020000";
        Integer numberOfSamplePoints = (number_of_requests < max_popular_points) ? number_of_requests : max_popular_points;

        return gson.toJson(getRandomPoints(numberOfSamplePoints, strokepickuppoints));
    }

    public String getBookingDistanceBins(Integer number_of_requests) {

        String result = "{";
        for (int i = 0; i < booking_distance_distribution.size(); i++) {
            String tempstring = "'From " + i + "->" + (i + 1) + "km':" + Math.round(booking_distance_distribution.get(i) * number_of_requests);
            if (i != booking_distance_distribution.size() - 1)
                tempstring += ",";
            result += tempstring;
        }

        return result + "}";
    }

    public List<Integer> getBookingDistanceBinsforinserting(Integer number_of_requests) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < booking_distance_distribution.size(); i++) {
            result.add((int) Math.round(booking_distance_distribution.get(i) * number_of_requests));

        }
        return result;
    }

    public Boolean pointLiesinBoundingBox(double longitude, double latitude) {


        if (bounding_box.max_longitude >= longitude && longitude >= bounding_box.min_longitude
                && bounding_box.min_latitude <= latitude && latitude <= bounding_box.max_latitude) {
            return true;

        }


        return false;
    }

}
