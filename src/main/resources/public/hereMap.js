
// Step 1: initialize communication with the platform
var platform = new H.service.Platform({
  apikey: 'c1LJuR0Bl2y02PefaQ2d8PvPnBKEN8KdhAOFYR_Bgmw'
});
var defaultLayers = platform.createDefaultLayers();

// Step 2: initialize a map
var map = new H.Map(document.getElementById('map'), defaultLayers.vector.normal.map, {
  zoom: 12,
  center: {lat: 52.522763341087874, lng: 13.492702024100026}, // for Berlin
  pixelRatio: window.devicePixelRatio || 1
});
// add a resize listener to make sure that the map occupies the whole container
window.addEventListener('resize', () => map.getViewPort().resize());


// Step 3: make the map interactive
// MapEvents enables the event system
// Behavior implements default interactions for pan/zoom (also on mobile touch environments)
var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

// Create the default UI components
var ui = H.ui.UI.createDefault(map, defaultLayers);

var urlpickup='http://localhost:8080/getridingpoolpickup?min_lon=13.34014892578125&min_lat=52.52791908000258&max_lon=13.506317138671875&max_lat=52.562995039558004&numberRequest=3';
var urldropoff='http://localhost:8080/getridingpooldropoff?min_lon=13.34014892578125&min_lat=52.52791908000258&max_lon=13.506317138671875&max_lat=52.562995039558004&numberRequest=3';


 async function init() {
                        let respurlpickup = await fetch(urlpickup);
                        let dataurlpickup = await respurlpickup.json();
                        var iconurlpickup = new H.map.Icon('pickup.png', {size: {w: 15, h: 15}});


                         let respurldropoff = await fetch(urldropoff);
                        let dataurldropoff = await respurldropoff.json();
                        var iconurldropoff = new H.map.Icon('dropoff.png', {size: {w: 15, h: 15}});


                         var group = new H.map.Group();
                          map.addObject(group);

                          group.addEventListener('tap', function (evt) {

                            var bubble =  new H.ui.InfoBubble(evt.target.getGeometry(), {
                              // read custom data
                              content: evt.target.getData()
                            });
                            // show info bubble
                            ui.addBubble(bubble);
                          }, false);

                        dataurlpickup.features.forEach(f => {

                        var marker = new H.map.Marker({lat:f.geometry.coordinates[1], lng:f.geometry.coordinates[0]}, { icon:iconurlpickup});
                         marker.setData(f.properties.name);
                         group.addObject(marker);

                        });


                       dataurldropoff.features.forEach(f => {

                        var marker = new H.map.Marker({lat:f.geometry.coordinates[1], lng:f.geometry.coordinates[0]}, { icon:iconurldropoff});
                         marker.setData(f.properties.name);
                         group.addObject(marker);

                        });
                        }

//init();




function process(){
var numberofrequest=$("#number_of_requests").val();
var countryname=$("#country_select").val();
showonmap(numberofrequest,countryname);

 $("#map").show();

}


async function showonmap(numberofrequest,countryname) {
   urlpickup="http://localhost:8080/getridingpoolpickupbycountry?countryName="+countryname+"&numberofrequest="+numberofrequest;
   urldropoff="http://localhost:8080/getridingpooldropoffcountry?countryName="+countryname+"&numberofrequest="+numberofrequest;

                        let respurlpickup = await fetch(urlpickup);
                        let dataurlpickup = await respurlpickup.json();
                        var iconurlpickup = new H.map.Icon('pickup.png', {size: {w: 15, h: 15}});


                         let respurldropoff = await fetch(urldropoff);
                        let dataurldropoff = await respurldropoff.json();
                        var iconurldropoff = new H.map.Icon('dropoff.png', {size: {w: 15, h: 15}});


                         var group = new H.map.Group();
                          map.addObject(group);

                          group.addEventListener('tap', function (evt) {

                            var bubble =  new H.ui.InfoBubble(evt.target.getGeometry(), {
                              // read custom data
                              content: evt.target.getData()
                            });
                            // show info bubble
                            ui.addBubble(bubble);
                          }, false);

                        dataurlpickup.features.forEach(f => {

                        var marker = new H.map.Marker({lat:f.geometry.coordinates[1], lng:f.geometry.coordinates[0]}, { icon:iconurlpickup});
                         marker.setData(f.properties.name);
                         group.addObject(marker);

                        });


                       dataurldropoff.features.forEach(f => {

                        var marker = new H.map.Marker({lat:f.geometry.coordinates[1], lng:f.geometry.coordinates[0]}, { icon:iconurldropoff});
                         marker.setData(f.properties.name);
                         group.addObject(marker);

                        });

                        }