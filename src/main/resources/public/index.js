window.onload = initialize;

var countryListArray;
async function initialize() {
    var Countryresponse= await execCommand("http://localhost:8080/getcountrynamesimulateddb" , "GET", null);//, function (data, status) {
        if (Countryresponse.status == 200 || Countryresponse.status == 201) {
                countryListArray=Countryresponse.data;
                console.log(countryListArray);
        } else {
                alert("Failed to update task");
        }


var $dropdownSetting = $(".contrySelect");
    $.each(countryListArray, function (key, value) {
        $dropdownSetting.
                append($("<option/>").
                    val(value).
                    text(value));
    });

 $("#map").hide();
}



function execCommand(url, method, data, callBack, nr) {
    return new Promise((ret, rej) => {
        var start_time = 0;
        var headers = { "Content-Type": "application/json; charset=utf-8" };
        $.ajax({
            url: url,
            type: method,
            headers: headers,
            data: data,
            contentType: "application/json; charset=utf-8",
            beforeSend: function (request, settings) {

                start_time = new Date().getTime();

            },
            success: function (data, textStatus, jqXHR) {
                var request_time = new Date().getTime() - start_time;
                ret({ status: jqXHR.status, data: data });
            },
            error: function (jqXHR, textStatus, errorThrown) {

                ret({ status: jqXHR.status, data: jqXHR.responseJSON });

            }
        });

    });
}