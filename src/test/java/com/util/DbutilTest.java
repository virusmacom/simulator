package com.util;

import com.helperobject.sqlitedb.Dbutil;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DbutilTest {
    @Test
    public void basicgetCountry() throws IOException {
        String returnvalue="[\"Berlin\",\"Cologne\",\"Munich\",\"Munster\"]";
        Assert.assertTrue(Dbutil.getCountryNameinSimulationDB().equals(returnvalue));
    }


    @Test
    public void getBoundingBoxCountry() throws IOException {
        String countryName="Berlin";
        BoundingBox boundingBox =Dbutil.getCountryboundingbox(countryName);
        Double max_lat=52.562995;
        Assert.assertTrue(boundingBox.max_latitude.equals(max_lat));

    }


    // Db check -- just add @Test to run this
    public void insertbookingdistanceinsqllite() throws IOException {
        String countryName="Berlin";
        List<Integer> data = new ArrayList<>();
        data.add(1);
        data.add(0);
        data.add(3);
        data.add(2);
        Dbutil.insertbookingdistanceinsqllite(countryName,data);

    }
}
