package com.util;

import com.helperobject.FeatureCollection0;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class HelperTest {
     Helper helper = null;


    @Test
    public void basicTest() throws IOException {
        BoundingBox bb = new BoundingBox(13.34014892578125, 52.52791908000258, 13.506317138671875, 52.562995039558004);
        helper= new Helper(bb);

        FeatureCollection0 featureCollection0= helper.getRandomPoints(5,"#E95C07"); //orange  #020000 black


        Assert.assertTrue(featureCollection0.getFeatures().size()==5);
    }
    @Test
    public void basicpointInBoundingTest() throws IOException {
        BoundingBox bb = new BoundingBox(13.34014892578125, 52.52791908000258, 13.506317138671875, 52.562995039558004);
        helper= new Helper(bb);
        Assert.assertTrue( helper.pointLiesinBoundingBox(13.45,52.53));

    }


    @Test
    public void basicbookingdistanceBinTest() throws IOException {
        BoundingBox bb = new BoundingBox(13.34014892578125, 52.52791908000258, 13.506317138671875, 52.562995039558004);
        helper= new Helper(bb);
        System.out.println(helper.getBookingDistanceBins(4));

    }
}
