import requests as req



def testalldatabaseinsimulateddb():
    resp = req.get("http://localhost:8080/getcountrynamesimulateddb")
    assert resp.status_code == 200
    response = resp.json();
    assert response[0] == 'Berlin'

def testcountryrequestcomplete():
    urlpickup = "http://localhost:8080/getridingpoolpickupbycountry?countryName=Berlin&numberofrequest=3"
    resp = req.get(urlpickup)
    response = resp.json();
    featurearray = response["features"];
    assert len(featurearray)==3

def testwithlatitudeandlongitude():
    urlpickup = 'http://localhost:8080/getridingpoolpickup?min_lon=13.34014892578125&min_lat=52.52791908000258&max_lon=13.506317138671875&max_lat=52.562995039558004&numberRequest=3';
    resp = req.get(urlpickup)
    response = resp.json();
    featurearray = response["features"];
    assert len(featurearray) == 3

testalldatabaseinsimulateddb()
testcountryrequestcomplete()
testwithlatitudeandlongitude()